<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>KOŞANLAR :: Madeni Yağlar - Filo Kilama - Garaj - Sigorta</title>
	
	<!-- core CSS -->
    <link href="<?=asset_url() ?>css/bootstrap.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=asset_url() ?>css/animate.min.css" rel="stylesheet">
    <link href="<?=asset_url() ?>css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=asset_url() ?>css/main.css" rel="stylesheet">
    <link href="<?=asset_url() ?>css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="<?=asset_url() ?>js/html5shiv.js"></script>
    <script src="<?=asset_url() ?>js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?=asset_url() ?>images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=asset_url() ?>images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=asset_url() ?>images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=asset_url() ?>images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?=asset_url() ?>images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +90 242 340 35 29 </p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="https://tr-tr.facebook.com/valvoline.kosanlar"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                          <!--  <div class="search">
                                <form role="form">
                                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                                    <i class="fa fa-search"></i>
                                </form>
                           </div> --> 
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Aç/Kapa</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?=base_url(); ?>"><img src="<?=asset_url(); ?>images/kosanlar_logo.png" alt="logo"></a>
                </div>
			
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <?=menu_anchor(base_url()."", "Anasayfa")?>
                        <?=menu_anchor(base_url()."hakkimizda", "Hakkımızda")?>
                        <?=menu_anchor(base_url()."madeni_yaglar", "Madeni Yağlar")?>
                        <?=menu_anchor(base_url()."filo_kiralama", "Filo Kiralama")?>
                        <?=menu_anchor(base_url()."garaj", "Garaj")?>
                        <?=menu_anchor(base_url()."sigorta", "Sigorta")?>
                        <?=menu_anchor(base_url()."iletisim", "İletişim")?>         <li><img src="<?=asset_url(); ?>images/mycar_logo.png" alt="logo"></li>               
                    </ul>
                  
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->