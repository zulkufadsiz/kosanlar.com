    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2014 <a target="_blank" href="http://www.kosanlar.com/" title="Koşanlar Madeni Yağ Filo Kiralama Garaj">Kosanlar.com</a>. Tüm hakları saklıdır.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="<?=base_url(); ?>">Anasayfa</a></li>
                        <li><a href="<?=base_url('hakkimizda'); ?>">Hakkımızda</a></li>
                        <li><a href="<?=base_url('iletisim'); ?>">İletişim</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="<?=asset_url() ?>js/jquery.js"></script>
    <script src="<?=asset_url() ?>js/bootstrap.min.js"></script>
    <script src="<?=asset_url() ?>js/jquery.prettyPhoto.js"></script>
    <script src="<?=asset_url() ?>js/jquery.isotope.min.js"></script>
    <script src="<?=asset_url() ?>js/main.js"></script>
    <script src="<?=asset_url() ?>js/wow.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56573514-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>