<section id="contact-info">
        <div class="container">
        <div class="center wow fadeInDown">          
            <h2>KOŞANLAR GARAJ</h2>
            <p class="lead">Öncelikle kendi filo araçlarımızın periyodik bakımları, partükül filitre cenerasyonu, boya kaporta, detaylı iç temizlik, klima gazı dolumu ve araçların arza tespit cihazı ile arzalarının bilgisayar ortamında giderilmesi.<br/>Generali Sigorta'nın yetkili anlaşmalı servisidir.</p>
        </div>
     
     </div>
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3189.361997654442!2d30.802553!3d36.92951600000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDU1JzQ2LjMiTiAzMMKwNDgnMDkuMiJF!5e0!3m2!1sen!2sus!4v1414922207595" width="400" height="300" frameborder="0" style="border:0"></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                                <address>
                                    <h5>Merkez Ofis</h5>
                                    <p>KOŞANLAR TUR. TAŞ. TİC. VE SAN. LTD. ŞTİ.<br/>
									Altınova Sinan Mah.Metinler Sok. <br/>
									No:5 K:2 ANTALYA
									</p>
                                    <p>Tel: +90 242 340 35 29 <br>
                                    Fax: +90 242 340 36 07 <br/>
                                    Email: info@kosanlar.com</p>
                                </address>
                            </li>


                            <li class="col-sm-6">
                            <!--     <address>
                                    <h5>Zone#2 Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                                 -->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->