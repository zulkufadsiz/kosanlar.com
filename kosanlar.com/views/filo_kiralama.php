<section id="contact-info">
	<div class="container">
		<div class="wow fadeInDown">
			<h2>PEKİ NEDEN ARAÇ KİRALAMA ?</h2>
			<p class="lead">Kurumsallaşmış veya bu yönde kurumsallaşmaya ve profesyonelleşmeye
			Doğru yol alan şirketler,kurum ve kuruluşlar Uzun Süreli Araç
			Kiralama Yoluyla hizmet alımlarını tercih etme nedenleri şunlardır.
			</p>
			<ol>
				<li>Sermaye çıkışınız olmaz</li>
				<li>Banka kredi limitlerinizi doldurmamış olacaksınız</li>
				<li>Banka kredi limitlerinizi ana ticari faaliyetiniz dışında
					kullanmamış olacaksınız.</li>
				<li>Bilançonuzda borç görünmemiş olacak.</li>
				<li>Banka faiz maliyetiniz olmamış olacak.</li>
				<li>Araç alımları için harcadığınız sermayeyi ana faaliyetinizde
					aylar ve yıllarca sirkülasyonlu bir şekilde ticaretinizde kullanmış
					olacaksınız.</li>
				<li>Binek araç satın aldığınızda ödediğiniz KDV’den
					yararlanamayacaksınız.</li>
				<li>Yani ödediğiniz KDV cepten gitmiş olacaktır.Halbuki araç
					kiralamayı tercih ederseniz kiralama faturasındaki KDV’den
					yararlanmış olacaksınız.uzun süreli kiralamada toplam&nbsp; KDV
					‘yi&nbsp; düşünürseniz şirketiniz acısından&nbsp; KDV’yi finansman
					olarak kullanmış olacaksınız.</li>
				<li>&nbsp;İster Binek ister ticari araç olsun, bu araçları satın
					aldığınızda Kurumlar ve gelir vergisi yönünden ancak 5 yıl boyunca
					gider (amortisman) yazabilirsiniz.</li>
				<li>Halbuki araç kiralamada direk muhasebe kayıtlarına gider
					yazabilirsiniz.Böylece şirketiniz %20 vergi avantajından işin
					başından itibaren yararlanmış olacaktır.</li>
				<li>Kasko ve Trafik poliçeleri maliyetiniz olmayacaktır.</li>
				<li>Yılda 2 defa motorlu taşıtlar vergisi maliyetiniz olmayacaktır.</li>
				<li>&nbsp;Servis-Bakım ve Onarım Maliyetiniz Olmayacaktır.<br></li>
			</ol>
			&nbsp;<br>
			<br>
			<br>

		</div>

	</div>
	<div class="gmap-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-5 text-center">
					<div class="gmap">
						<iframe
							src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3189.361997654442!2d30.802553!3d36.92951600000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDU1JzQ2LjMiTiAzMMKwNDgnMDkuMiJF!5e0!3m2!1sen!2sus!4v1414922207595"
							width="400" height="300" frameborder="0" style="border: 0"></iframe>
					</div>
				</div>

				<div class="col-sm-7 map-content">
					<ul class="row">
						<li class="col-sm-6">
							<address>
								<h5>Merkez Ofis</h5>
								<p>
									KOŞANLAR TUR. TAŞ. TİC. VE SAN. LTD. ŞTİ.<br /> Altınova Sinan
									Mah.Metinler Sok. <br /> No:5 K:2 ANTALYA
								</p>
								<p>
									Tel: +90 242 340 35 29 <br> Fax: +90 242 340 36 07 <br />
									Email: info@kosanlar.com
								</p>
							</address>
						</li>


						<li class="col-sm-6">
							<!--     <address>
                                    <h5>Zone#2 Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                                 -->
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/gmap_area -->