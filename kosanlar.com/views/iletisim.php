<section id="contact-info">
        <div class="container">
         <div class="center wow fadeInDown">          
            <h2>VİZYONUMUZ</h2>
            <p class="lead">Bölgemizde var olan zengin  Turizm kaynaklarını ve değerlerini en verimli şekilde kullanarak insanlığa hizmet eder hale getirirken, bölgemizde lider, bölgesel ekonomide ise etkin ve saygın bir oyuncu olmak. İnsan, kalite, teknoloji, yenilik ve verimlilik odaklı bir yönetim stratejisi ile ürün ve hizmetlerimizi sürekli geliştirerek, müşterilerimize en yüksek değeri yaratmak...</p>
        </div>
     </div>
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m20!1m8!1m3!1d1340.2113490190059!2d30.77697026560767!3d36.972140393509044!3m2!1i1024!2i768!4f13.1!4m9!3e6!4m3!3m2!1d36.9721818!2d30.776974799999998!4m3!3m2!1d36.972189!2d30.7770515!5e0!3m2!1str!2s!4v1441190894559" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                                <address>
                                    <h5>Merkez Ofis</h5>
                                    <p>KOŞANLAR TUR. TAŞ. TİC. VE SAN. LTD. ŞTİ.<br/>
									Gaziler Mah. S.S.Antalya Esnaf Küçük San. Sit. <br/>
									248.Sok. No:2-4 Kepez- ANTALYA
									</p>
                                    <p>Tel: +90 242 340 35 29 <br>
                                    Fax: +90 242 340 36 07 <br/>
                                    Email: info@kosanlar.com.tr</p>
                                </address>
                            </li>


                            <li class="col-sm-6">
                            <!--     <address>
                                    <h5>Zone#2 Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                                 -->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->