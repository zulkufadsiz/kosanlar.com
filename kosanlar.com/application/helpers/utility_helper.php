<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('asset_url()'))
{
	function asset_url($uri = "")
	{
		if ($uri != "")
			return base_url("assets/".$uri);
		else
			return base_url().'assets/';
	}
}