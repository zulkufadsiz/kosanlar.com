﻿
<section id="feature">
	<div class="container">
		<div class="center wow fadeInDown">
			<h2>HİZMETLERİMİZ</h2>
			<p class="lead">
				Akdeniz Bölgesi başta olmak üzere tüm Türkiye'ye madeni yağ, filo
				kiralama, araç bakım, <BR />kaporta boya servisi ve sigorta
				alanlarında hizmet vermekteyiz.
			</p>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-6 wow fadeInDown"
				data-wow-duration="1000ms" data-wow-delay="600ms" onclick="location.href='/madeni_yaglar'">
				<div class="feature-wrap">
					<i class="fa fa-dashboard"></i>
					<h2>Madeni Yağlar</h2>
					<h3>Otomotiv Yağları, Motorsiklet Yağları, Endüstriyel Yağlar</h3>
				</div>
			</div>
			<!--/.col-md-4-->

			<div class="col-md-4 col-sm-6 wow fadeInDown"
				data-wow-duration="1000ms" data-wow-delay="600ms" onclick="location.href='/filo_kiralama'">
				<div class="feature-wrap">
					<i class="fa fa-car"></i>
					<h2>Filo Kiralama</h2>
					<h3>Firmanızın beklentilerine uygun, kaliteli ve hesaplı filo
						kiralama</h3>
				</div>
			</div>
			<!--/.col-md-4-->

			<div class="col-md-4 col-sm-6 wow fadeInDown"
				data-wow-duration="1000ms" data-wow-delay="600ms" onclick="location.href='/garaj'">
				<div class="feature-wrap">
					<i class="fa fa-cogs"></i>
					<h2>Garaj</h2>
					<h3>Mekanik bakım, kaporta boya, plastik aksam tamirleri</h3>
				</div>
			</div>
			<!--/.col-md-4-->
			<div class="col-md-4 col-md-offset-4 col-sm-offset-6 col-sm-6 wow fadeInDown"
				data-wow-duration="1000ms" data-wow-delay="600ms" onclick="location.href='/sigorta'">
				<div class="feature-wrap">
					<i class="fa fa-heart"></i>
					<h2>Sigorta</h2>
					<h3>Araç, Kasko ve Trafik Sigortaları, Konuk Sigortaları (DASK),
						İşyeri ve Site Sigortaları, Seyahat Sigortaları</h3>
				</div>
			</div>
			<!--/.col-md-4-->
		</div>
		<!--/.services-->
	</div>
	<!--/.row-->
	</div>
	<!--/.container-->
</section>
<!--/#feature-->

<section id="recent-works">
	<div class="container">
		<div class="center wow fadeInDown">
			<h2>BÖLGE DİSTRİBÜTÖRLÜKLERİMİZ & BAYİLİKLERİMİZ</h2>
			<p class="lead">Uluslararası kabul görmüş madeni yağların Akdeniz
				bölgesi distribütörlük ve bayiliklerini yapmaktayız.</p>
		</div>

		<div class="row">

			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="recent-work-wrap">
					<a href="<?=base_url("motul") ?>"><img class="img-responsive"
						src="<?=asset_url();?>images/motul_290x200.png" alt="Motul"></a>
				</div>
			</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="recent-work-wrap">
					<a href="<?=base_url("texol") ?>"><img class="img-responsive"
						src="<?=asset_url();?>images/texol_290x200.png" alt="Texol"></a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="recent-work-wrap">
					<a href="<?=base_url("speedol") ?>"><img class="img-responsive"
						src="<?=asset_url();?>images/speedol.png" alt="Speedol">
				</div>
			</div>
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
</section>
<!--/#recent-works-->

<section id="services" class="service-item">
	<div class="container">
		<div class="center wow fadeInDown">
			<h2>FİLO ARAÇLARIMIZ</h2>
			<p class="lead">Her daim yenilenmiş filolarımızla, her türlü araç
				ihtiyacınız karşılanır.</p>
		</div>

		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">
					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url('images/filo/a4.png');?>">
						<h3 class="media-heading">Audi A4</h3>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">
					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url('images/filo/a5.png');?>">
						<h3 class="media-heading">Audi A5</h3>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">
					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url('images/filo/320d.png');?>">
						<h3 class="media-heading">BMW 320D</h3>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">

					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url('images/filo/astra.png');?>">
						<h3 class="media-heading">Opel Astra</h3>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">

					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url();?>images/filo/corsa.png">
						<h3 class="media-heading">Opel Corsa</h3>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">

					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url();?>images/filo/fluence.png">
						<h3 class="media-heading">Renault Fluence</h3>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">

					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url();?>images/filo/albea.png">
						<h3 class="media-heading">Fiat Albea</h3>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">

					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url();?>images/filo/clio.png">
						<h3 class="media-heading">Renault Clio</h3>
					</div>
				</div>
			</div>




			<div class="col-sm-6 col-md-4">
				<div class="media services-wrap wow fadeInDown">
					<div class="media-body">
						<img class="img-responsive"
							src="<?=asset_url();?>images/filo/ducado.jpg">
						<h3 class="media-heading">Ducado</h3>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
</section>
<!--/#services-->
<section id="conatcat-info">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<div class="media contact-info wow fadeInDown"
					data-wow-duration="1000ms" data-wow-delay="600ms">
					<div class="pull-left">
						<i class="fa fa-phone"></i>
					</div>
					<div class="media-body">
						<h2>Bize 7x24 ulaşabilirsiniz!</h2>
						<p>
							Kaliteli hizmetin, sadece ürün satılasıya kadar değil, sonrasında
							da aynı hasasiyet ve özveriyle olması gerektiğine inanıyoruz. Tüm
							hizmet alanlarımızda 7 gün 24 saat yanınızdayız.<br /> +90 242
							340 35 29
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/.container-->
</section>
<!--/#conatcat-info-->
<section id="bottom">
	<div class="gmap-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-5 text-center">
					<div class="gmap">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3187.4521600359185!2d30.7787222!3d36.975138900000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDU4JzMwLjUiTiAzMMKwNDYnNDMuNCJF!5e0!3m2!1str!2s!4v1431718058204" width=“400" height=“300" frameborder="0" style="border:0"></iframe>
					</div>
				</div>

				<div class="col-sm-7 map-content">
					<ul class="row">
						<li class="col-sm-6">
							<address>
								<h5>Merkez Ofis</h5>
								<p>
									KOŞANLAR TUR. TAŞ. TİC. VE SAN. LTD. ŞTİ.<br />Gaziler Mah. S.S Esnaflar Küçük Sanayi Sitesi <br/>
									248 Sokak No.2-4 Kepez- ANTALYA								</p>
								<p>
									Tel: +90 242 340 35 29 <br> Fax: +90 242 340 36 07 <br />
									Email: info@kosanlar.com
								</p>
							</address>
						</li>


						<li class="col-sm-6">
							<!--     <address>
                                    <h5>Zone#2 Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                                 -->
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>