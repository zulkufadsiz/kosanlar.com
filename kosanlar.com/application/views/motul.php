<section id="contact-info">
        <div class="container">
        	<div class="row ">
        		<div class="col-xs-12 list-modul">
        			<? if ($parent_cat==null): ?>
        			<ul class="list-inline">
        				<li class="col-xs-3" >
        					<h3>MOTORSİKLET</h3>
        					<a href="<?=base_url('motul') ?>/motorsiklet"><img src="<?=asset_url() ?>images/motul/1.jpg" /></a>
        				</li>
        				<li class="col-xs-3">
        					<h3>MARIN</h3>
        					<a href="<?=base_url('motul') ?>/marin"><img src="<?=asset_url() ?>images/motul/2.jpg" /></a>
        				</li>
        				<li class="col-xs-3">
        					<h3>GO KART</h3>
        					<a href="<?=base_url('motul') ?>/go-kart"><img src="<?=asset_url() ?>images/motul/3.jpg" /></a>
        				</li>
        			</ul>
        			<? endif; ?>
        			<? if ($motorsiklet!=null && $cat==null): ?>
        			<ul class="list-inline">
        				<?php     			
        				foreach ($motorsiklet as $_motorsiklet):
						?>
							<li class="col-xs-3" >
	        					<h3><?=$_motorsiklet["name"] ?></h3>
	        					<a href="<?=base_url('motul') ?>?parent_cat=motorsiklet&cat=<?=$_motorsiklet["slug"] ?>"><img src="<?=asset_url() ?>images/motul/<?=$_motorsiklet["image"] ?>" /></a>
        					</li>
						<?php
						endforeach;
						?>
        			</ul>
        			<? endif; ?>
        			<? if ($marin!=null && $cat==null): ?>
        			<ul class="list-inline">
        				<?php     			
        				foreach ($marin as $_marin):
						?>
							<li class="col-xs-3" >
	        					<h3><?=$_marin["name"] ?></h3>
	        					<a href="<?=base_url('motul') ?>?parent_cat=marin&cat=<?=$_marin["slug"] ?>"><img src="<?=asset_url() ?>images/motul/<?=$_marin["image"] ?>" /></a>
        					</li>
						<?php
						endforeach;
						?>
        			</ul>
        			<? endif; ?>
        			
        			<? if ($cat!=null && $products!=null): ?>
        				<?
        						
			        				foreach ($products as $_product):
									?>
										<li class="col-xs-3" >
				        					<h3><?=$_product["name"] ?></h3>
				        					<a href="<?=base_url('motul') ?>?parent_cat=motorsiklet&cat=<?=$cat ?>&product=<?=$_product["slug"] ?>"><img src="<?=asset_url() ?>images/motul/<?=$_product["image"] ?>" /></a>
			        					</li>
									<?php
									endforeach;
									?>
        				?>	
        			<? endif; ?>
        		</div>
        	</div>
     </div>
</section>