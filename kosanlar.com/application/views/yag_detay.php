<section id="oil-details">
	<div class="container">
	
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<img class="img-thumbnail img-responsive" src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF 600">
		</div>
		<div class="col-sm-12 col-md-6">
			<table class="table table-condensed">
				<caption><h4>RBF 600 Brake Fluid</h4></caption>
				<thead>
					<tr>
						<th>Ürün Özellikleri</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Hidrolik fren ve debriyajlar için 100% sentetik hidrolik sıvısı
						Kuru kaynama noktası 312&#8451;
						</td>
					</tr>
				</tbody>
			</table>
			
				<a class="btn btn-primary pull-right" id="yorum_yap0" href="#comments">Yorum Yap</a>
			
		</div>

	</div>
	

	</div>

	
</section>
<section id="comments">
	
	<div class="container">
		
		<div class="row">
		
	<div class="col-sm-12 col-md-8">
		
	<div class="cmt-head">
		<div><label for="ad">Ad Soyad</label>
		
			<input type="text" class="form-control " id="ad"  placeholder="Ad Soyad">
		</div>
	</div>
	<div class="cmt-body">
		<div><label for="ad">Yorum</label>
		
			<textarea class="form-control" placeholder="Yorum yaz" rows="3"></textarea>
		</div>
		<a class="btn btn-primary pull-right" id="yorum_yap1" href="">Yorum Yap</a>
	</div>

	</div>
	<div class="col-sm-12 col-md-4">
			<div class="caption text-center">
				<h5>İlginizi çekebilecek ürünler</h5>
			</div>
		<div class="img-thumbnail out-line">
		
		<div class="row">
			
			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail" src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

		</div>
		<div class="row">
			
			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail" src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

		</div>

		<div class="row">
			
			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail" src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

		</div>
		<div class="row">
			
			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail"  src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>

			<div class="col-sm-6 col-md-4">			
			<img class="img-responsive image img-thumbnail" src="<?=asset_url('images/portfolio/masonry/5/rbf_600_brake_fluid_0500l.png'); ?>" alt="RBF660">
			</div>


		</div>
		</div>


	</div>


	</div>
	<div class="row">
		<div class="col-sm-12 col-md-8 cmnt-border">
		<div class="comments">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

		
		</div>
		<div class="like">
			<i href="#" class="fa fa-thumbs-o-down">Olumsuz</i><i href="#" class="fa fa-thumbs-o-up">Olumlu</i>
		</div>
		<div>
			
		</div>
	</div>
	</div>
	</div>

</section>