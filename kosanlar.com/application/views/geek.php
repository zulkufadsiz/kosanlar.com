<section id="admin">
	<div class="form-horizontal">
  <fieldset>
    <legend>Ürün Bilgi Girişi</legend>
  <?php echo form_open_multipart('doUpload');?>
  <table>
  <tr>
    <div class="form-group">
      <label for="urunadi" class="col-lg-2 control-label">Ürün Adı</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" name="title" id="urunadi" placeholder="Ürün Adı">
      </div>
    </div>
 </tr>
<!-- <?php echo $error;?> -->



  <tr>
    <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Açıklama</label>
      <div class="col-lg-10">
        <textarea class="form-control" name="aciklama" rows="3" id="textArea"></textarea>
      </div>
    </div>
  </tr>
  <tr>
    <div class="form-group">
    <label for="foto" class="col-lg-2 control-label">Resim Yükle</label>
    <div class="col-lg-10">
      <input type="file" name="userfile" size="20" id="foto">
      <p class="help-block">Fotoğraflar png veya jpg olabilir.</p>
    </div>   
    </div>
  </tr>
  <tr>
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-default">İptal</button>
        <button type="submit" class="btn btn-primary">Upload</button>
      </div>
    </div>
  </tr>  
    </table>
    </div>
  </fieldset>
</form>
</section>