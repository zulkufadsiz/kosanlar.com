
    <section>
        
    
<div class="container">
    

<form class="form-horizontal">
  <fieldset>
    <legend>Ürün Ekle</legend>
    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Marka</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="marka" placeholder="Marka">
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Ürün Adı</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="urunadi" placeholder="Ürün Adı">
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Ürün Kodu</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="urunkodu" placeholder="Ürün Kodu">
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Ürün Türü</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="urunturu" placeholder="Ürün Türü">
      </div>
    </div>

   
    
    <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Açıklama</label>
      <div class="col-lg-10">
        <textarea class="form-control" rows="3" id="aciklama"></textarea>
        <span class="help-block">Ürün açıklamasını giriniz</span>
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <label class="control-label">Küçük Resim</label>
        <input name="kucuk" id="input-1" type="file" class="file">
      </div>
    </div>
    
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <label class="control-label">Büyük Resim</label>
        <input name="buyuk" id="input-2" type="file" class="file">
      </div>
    </div>
    
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-warning">İptal</button>
        <button type="submit" class="btn btn-primary">Kaydet</button>
      </div>
    </div>
  </fieldset>
</form>


</div>


</section>
