<section id="contact-info">
        <div class="container">
        <div class="center wow fadeInDown">          
            <h2>HAKKIMIZDA</h2>
            <p class="lead">1992 yılında kurulan KOŞANLAR LTD. , MyCAR , KOŞANLAR GARAJ markalarıyla turizmde bilgi ve deneyimiyle bugüne gelen firmamız günümüzün teknolojik gelişmelerini takip ederek sizlere hızlı, kaliteli ve uygun fiyatlarıyla en iyi hizmeti vermek için çalışmalarını sürdürmektedir. <br/> 2013 yılından itibaren uluslararası kabul görmüş madeni yağların distribütörlüklerini alarak Akdeniz bölgesine hizmet vermektedir.</p>
        </div>
         <div class="center wow fadeInDown">          
            <h2>VİZYONUMUZ</h2>
            <p class="lead">Bölgemizde var olan zengin  Turizm kaynaklarını ve değerlerini en verimli şekilde kullanarak insanlığa hizmet eder hale getirirken, bölgemizde lider, bölgesel ekonomide ise etkin ve saygın bir oyuncu olmak. İnsan, kalite, teknoloji, yenilik ve verimlilik odaklı bir yönetim stratejisi ile ürün ve hizmetlerimizi sürekli geliştirerek, müşterilerimize en yüksek değeri yaratmak...</p>
        </div>
     </div>
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3187.4521600359185!2d30.7787222!3d36.975138900000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDU4JzMwLjUiTiAzMMKwNDYnNDMuNCJF!5e0!3m2!1str!2s!4v1431718058204" width=“400" height=“300" frameborder="0" style="border:0"></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                               <address>
                                    <h5>Merkez Ofis</h5>
                                    <p>KOŞANLAR TUR. TAŞ. TİC. VE SAN. LTD. ŞTİ.<br/>
									Gaziler Mah. S.S.Antalya Esnaf Küçük San. Sit. <br/>
									248.Sok. No:2-4 Kepez- ANTALYA
									</p>
                                    <p>Tel: +90 242 340 35 29 <br>
                                    Fax: +90 242 340 36 07 <br/>
                                    Email: info@kosanlar.com.tr</p>
                                </address>
                            </li>


                            <li class="col-sm-6">
                            <!--     <address>
                                    <h5>Zone#2 Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                                 -->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->