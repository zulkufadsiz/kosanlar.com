<?



$p1_1_name = "MOTUL 2T YAĞLAR";
$p1_1_slug = "motul_2t_yaglar";
$p1_1_image = "1_1.jpg";
$p1_1_content ="";

$p1_2_name = "MOTUL 4T YAĞLAR";
$p1_2_slug = "motul_4t_yaglar";
$p1_2_image = "1_2.jpg";
$p1_2_content ="";

$p1_3_name = "MOTUL FACTORY LINE";
$p1_3_slug = "motul_factory_line";
$p1_3_image = "1_3.png";
$p1_3_content ="";

$p1_4_name = "MOTUL ATV YAĞLARI";
$p1_4_slug = "motul_atv_yaglari";
$p1_4_image = "1_4.jpg";
$p1_4_content ="";

$p1_5_name = "MOTUL KAR MOTORU YAĞLARI";
$p1_5_slug = "motul_kar_motoru_yaglari";
$p1_5_image = "1_5.jpg";
$p1_5_content ="";

$p1_6_name = "MOTUL AMORTİSÖR YAĞLARI";
$p1_6_slug = "motul_amortisor_yaglari";
$p1_6_image = "1_6.jpg";
$p1_6_content ="";

$p1_7_name = "MOTUL ŞANZIMAN YAĞLARI";
$p1_7_slug = "motul_sanziman_yaglari";
$p1_7_image = "1_7.jpg";
$p1_7_content ="";

$p1_8_name = "MOTUL HİDROLİK SIVILARI";
$p1_8_slug = "motul_hidrolik_yaglari";
$p1_8_image = "1_8.jpg";
$p1_8_content ="";

$p1_9_name = "MOTUL SOĞUTMA SIVILARI";
$p1_9_slug = "motul_sogutma_sivilari";
$p1_9_image = "1_9.jpg";
$p1_9_content ="";

$p1_10_name = "MOTUL MC CARE (BAKIM ÜRÜNLERİ)";
$p1_10_slug = "motul_mc_care_bakim_urunleri";
$p1_10_image = "1_10.jpg";
$p1_10_content ="";


$p1_1_1_name = "MOTUL 100 SERİSİ";
$p1_1_1_slug = "motul_100_serisi";
$p1_1_1_image = "1_1_1.png";
$p1_1_1_content = '<p style="text-align: left;"><font size="2">&nbsp;<img src="http://www.k-ma.com.tr/UPLOAD/E/100.png" alt="" height="279" width="162"></font><br><br><font size="2">MOTO MIX 2T mineral bazlı, son jenerasyon moped motorları için üretilmiş 2 zamanlı motor yağıdır. Yakıtla çok kolayca ve anında karışır. MOTO MIX 2T üreticilerin talepleri doğrultusunda üretilmiştir. MOTO MIX 2T karbon birikimini azaltır ve buji bozulmalarını minimize eder.&nbsp;Katalitik konvertörlerle uyumludur.</font><br><br><strong style="font-size: small;">İçerik: </strong><font size="2">Mineral bazlı. Bujilerin bozulmalarını engeller ve egzozta birikime izin vermez.</font><br><strong style="font-size: small;">Standartlar:</strong><font size="2"> API TC/TSC3 &amp; JASO FB kalitesi</font><br><strong style="font-size: small;">Uyumluluk:</strong><font size="2"> Hem hazneli hem de yakıta karıştırmalı yağlama sistemlerine sahip tüm 2 zamanlı moped motorlarıyla uyumludur.</font><br><br><font size="2"><b>Ambalajlar:</b> 1L kutu</font></p>';

$p1_1_2_name = "MOTUL 510 SERİSİ";
$p1_1_2_slug = "motul_510_serisi";
$p1_1_2_image = "1_1_2.png";
$p1_1_2_content = "";

$p1_1_3_name = "MOTUL 710 SERİSİ";
$p1_1_3_slug = "motul_710_serisi";
$p1_1_3_image = "1_1_3.png";
$p1_1_3_content = "";

$p1_1_4_name = "MOTUL 800 SERİSİ (YARIŞ SERİSİ)";
$p1_1_4_slug = "motul_800_serisi_yaris_serisi";
$p1_1_4_image = "1_1_4.png";
$p1_1_4_content = "";

$p1_1_5_name = "MOTUL SCOOTER SERİSİ";
$p1_1_5_slug = "motul_scooter_serisi";
$p1_1_5_image = "1_1_5.png";
$p1_1_5_content = "";

/* 2 */

$p2_1_name = "IN BOARD";
$p2_1_slug = "in_board";
$p2_1_image = "2_1.jpg";
$p2_1_content ="";

$p2_2_name = "OUT BOARD";
$p2_2_slug = "out_board";
$p2_2_image = "2_2.jpg";
$p2_2_content ="";

$p2_3_name = "JETSKI";
$p2_3_slug = "jetski";
$p2_3_image = "2_3.jpg";
$p2_3_content ="";
