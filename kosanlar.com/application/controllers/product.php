<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product extends CI_Controller {
	public function product_by_id($id){
		$this->load->model('product_model', 'mProduct');
		$data['products'] = $this->mProduct->getProductsByParentId($id);
		$data['product'] = $this->mProduct->getProductById($id);
		$this->load->view('include/header');
		if($data['products']!=null && count($data['products']))
			$this->load->view('products_page',$data);
		else
			$this->load->view('product_page',$data);
		$this->load->view('include/footer');
	}
	
	public function motul_categories(){
		$this->load->view('include/header');
		$this->load->view('category_page');
		$this->load->view('include/footer');
	}
	
	public function products_by_brand_and_category($brandId,$categoryId)
	{
		$this->load->model('product_model', 'mProduct');
		$data['products'] = $this->mProduct->getProductsByBrandIdAndCategoryId($brandId,$categoryId);
		$this->load->view('include/header');
		$this->load->view('products_page',$data);
		$this->load->view('include/footer');
	}
	
}