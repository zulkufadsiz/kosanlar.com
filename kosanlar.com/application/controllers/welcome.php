<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once "products.php";
class Welcome extends CI_Controller {



	public function __construct() {
		
		parent::__construct();

		//
	}


	public function index()
	{
		$this->load->view('include/header');
		$this->load->view('welcome_message');
		$this->load->view('include/footer');
		
		$this->load->helper(array('form', 'url'));
	}
	public function hakkimizda()
	{
		$this->load->view('include/header');
		$this->load->view('hakkimizda');
		$this->load->view('include/footer');
	}
	
	public function iletisim()
	{
		$this->load->view('include/header');
		$this->load->view('iletisim');
		$this->load->view('include/footer');
	}
	
	public function madeni_yaglar()
	{
		$this->load->view('include/header');
		$this->load->view('madeni');
		$this->load->view('include/footer');
	}

	
	public function filo_kiralama()
	{
		$this->load->view('include/header');
		$this->load->view('filo_kiralama');
		$this->load->view('include/footer');
	}
	
	
	public function garaj()
	{
		$this->load->view('include/header');
		$this->load->view('garaj');
		$this->load->view('include/footer');
	}
	
	public function sigorta()
	{
		$this->load->view('include/header');
		$this->load->view('hakkimizda');
		$this->load->view('include/footer');
	}
	
	public function yag_detay()
	{
		$this->load->view('include/header');
		$this->load->view('yag_detay');
		$this->load->view('include/footer');
	}
	
	public function yonetici()
	{
		$this->load->view('include/header');
		$this->load->view('yonetici');
		$this->load->view('include/footer');
		
	}

	/*public function do_upload(){

		$config['upload_path'] = './assets/images/urun_gorsel/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			//$error = array('error' => $this->upload->display_errors());
			$this->load->view('include/header');
			$this->load->view('geek');
			$this->load->view('include/footer');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$this->load->view('upload_success', $data);
		}

	}
*/


	 public function doUpload() {
	 	$this->load->helper(array('form', 'url'));
	 	$this->load->view('include/header');
	 	$this->load->view('geek');
	 	$this->load->view('include/footer');
	 	$config['upload_path'] = './assets/images/urun_gorsel/';
	 	$config['allowed_types'] = 'gif|jpg|jpeg|png';
	 	$config['max_size'] = 100;
	 	$config['max_width'] =1024;
	 	$config['max_height'] =768;
	 	$this->load->library('upload',$config);
    	if (isset($_POST["title"])&& isset($_POST["aciklama"])) {
    		// echo $_POST["title"];
    		// echo $_POST["aciklama"];
    	}
    	if ($this->upload->do_upload('userfile')) {
      		$data  = array('upload_data' =>$this->upload->data());
    		print_r($data);
    		$this->load->model('product_model');
    		$this->product_model->save($data);

    	}
    	
   } 



	public function motul_ex()
	{
		global $p1_1_name, $p1_1_image, $p1_1_content, $p1_1_slug;
		global $p1_2_name, $p1_2_image, $p1_2_content, $p1_2_slug;
		global $p1_3_name, $p1_3_image, $p1_3_content, $p1_3_slug;
		global $p1_4_name, $p1_4_image, $p1_4_content, $p1_4_slug;
		global $p1_5_name, $p1_5_image, $p1_5_content, $p1_5_slug;
		global $p1_6_name, $p1_6_image, $p1_6_content, $p1_6_slug;
		global $p1_7_name, $p1_7_image, $p1_7_content, $p1_7_slug;
		global $p1_8_name, $p1_8_image, $p1_8_content, $p1_8_slug;
		global $p1_9_name, $p1_9_image, $p1_9_content, $p1_9_slug;
		global $p1_10_name, $p1_10_image, $p1_10_content, $p1_10_slug;
		global $p2_1_name, $p2_1_image, $p2_1_content, $p2_1_slug;
		global $p2_2_name, $p2_2_image, $p2_2_content, $p2_2_slug;
		global $p2_3_name, $p2_3_image, $p2_3_content, $p2_3_slug;
		global $p1_1_1_name, $p1_1_1_image, $p1_1_1_content, $p1_1_1_slug;
		global $p1_1_2_name, $p1_1_2_image, $p1_1_2_content, $p1_1_2_slug;
		global $p1_1_3_name, $p1_1_3_image, $p1_1_3_content, $p1_1_3_slug;
		global $p1_1_4_name, $p1_1_4_image, $p1_1_4_content, $p1_1_4_slug;
		global $p1_1_5_name, $p1_1_5_image, $p1_1_5_content, $p1_1_5_slug;
		


		$data = array();
		$data['parent_cat'] = $this->input->get('parent_cat', TRUE);
		$data['cat'] = $this->input->get('cat', TRUE); //(isset($this->input->get('cat', TRUE)))?$this->input->get('cat', TRUE):null;
		
		
		$data["motorsiklet"] = null;
		$data["marin"] = null;
		$data['products'] = null;
		if(strcmp($data['parent_cat'],"motorsiklet")==0){
			$motorsiklet = array(
				array("name"=>$p1_1_name,
					  "image"=>$p1_1_image,
					  "slug"=>$p1_1_slug
					  ),
				array("name"=>$p1_2_name,
					  "image"=>$p1_2_image,
					  "slug"=>$p1_2_slug
					  ),
				array("name"=>$p1_3_name,
					  "image"=>$p1_3_image,
					  "slug"=>$p1_3_slug
					  ),
				array("name"=>$p1_4_name,
					  "image"=>$p1_4_image,
					  "slug"=>$p1_4_slug
					  ),
			    array("name"=>$p1_5_name,
					  "image"=>$p1_5_image,
					  "slug"=>$p1_5_slug
					  ),
				array("name"=>$p1_6_name,
					  "image"=>$p1_6_image,
					  "slug"=>$p1_6_slug
					  ),
				array("name"=>$p1_7_name,
					  "image"=>$p1_7_image,
					  "slug"=>$p1_7_slug
					  ),
				array("name"=>$p1_8_name,
					  "image"=>$p1_8_image,
					  "slug"=>$p1_8_slug
					  ),
				array("name"=>$p1_9_name,
					  "image"=>$p1_9_image,
					  "slug"=>$p1_9_slug
					  ),
				array("name"=>$p1_10_name,
					  "image"=>$p1_10_image,
					  "slug"=>$p1_10_slug
					  )
				

			
			);
							$data["motorsiklet"] = $motorsiklet;
			}
			if(strcmp($data['parent_cat'],"marin")==0){
				$marin = array(
					array("name"=>$p2_1_name,
						  "image"=>$p2_1_image,
						  "slug"=>$p2_1_slug
						  ),
					array("name"=>$p2_2_name,
						  "image"=>$p2_2_image,
						  "slug"=>$p2_2_slug
						  ),
					array("name"=>$p2_3_name,
						  "image"=>$p2_3_image,
						  "slug"=>$p2_3_slug
						  )
				);
			

		$data["marin"] = $marin;
			
		}
	
			if(strcmp($data['cat'],"motul_2t_yaglar")==0){
			$products = array(
				array("name"=>$p1_1_1_name,
					  "image"=>$p1_1_1_image,
					  "slug"=>$p1_1_1_slug,
					  "content"=>$p1_1_1_content
					  ),
				array("name"=>$p1_1_2_name,
					  "image"=>$p1_1_2_image,
					  "slug"=>$p1_1_2_slug,
					  "content"=>$p1_1_2_content
					  ),
				array("name"=>$p1_1_3_name,
					  "image"=>$p1_1_3_image,
					  "slug"=>$p1_1_3_slug,
					  "content"=>$p1_1_3_content
					  ),
				array("name"=>$p1_1_4_name,
					  "image"=>$p1_1_4_image,
					  "slug"=>$p1_1_4_slug,
					  "content"=>$p1_1_4_content
					  ),
				array("name"=>$p1_1_5_name,
					  "image"=>$p1_1_5_image,
					  "slug"=>$p1_1_5_slug,
					  "content"=>$p1_1_5_content
					  )
			
			);
							$data["products"] = $products;
			}
			
	
	
		
		$this->load->view('include/header');
		$this->load->view('motul',$data);
		$this->load->view('include/footer');
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */